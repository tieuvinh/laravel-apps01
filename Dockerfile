# Set master image
FROM php:8.1-fpm

# Set working directory
WORKDIR /var/www/html

RUN apt-get update \
    && apt-get install -y zlib1g-dev libpq-dev git libicu-dev libxml2-dev libcurl4-openssl-dev \
    pkg-config libssl-dev libzip-dev zlib1g-dev \
    libfreetype6-dev libjpeg62-turbo-dev libpng-dev

# Cài đặt các tool cần thiết
RUN apt-get update && apt-get install -y \
  git \
  curl \
  vim \
  nano \
  net-tools \
  pkg-config \
  iputils-ping \
  apt-utils \
  zip \
  unzip \
  supervisor \
  && rm -rf /var/lib/apt/lists/*

# Change timezone
RUN echo "Asia/Ho_Chi_Minh" > /etc/timezone

# Cài đặt các thư viện cần thiết
RUN docker-php-ext-install \
  bcmath \
  pdo_mysql \
  gd

# Install PHP Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# RUN chown -R www-data:www-data storage
# # Copy existing application directory

COPY ./laravel .

RUN chown -R www-data:www-data storage

# Set up supervisor configuration
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
