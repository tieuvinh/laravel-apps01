# install laravel app docker

Chạy cài đặt composer install

```
cd laravel
docker run --rm -v $(pwd):/app -w /app composer install
```
Quay trở lại thư mục gốc của App, chạy docker compose

`docker-compose up -d`

Chạy tiếp lệnh gen key

`docker-compose exec app php artisan key:generate`

Vào trình duyệt ``http://localhost:8888`` xem kết quả

Nếu bị lỗi Permission thì thực hiện exec vào container hoặc thực hiện exec từ ngoài như sau

`docker-compose exec app chown -R www-data:www-data storage`



